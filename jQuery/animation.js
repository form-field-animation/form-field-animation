jQuery(function() {
	jQuery("input,textarea").on("click", function(e) {
    	jQuery(this).parent().siblings().addClass("shift");
    	e.stopPropagation()
  	});

	jQuery(document).on("click", function(e) {
  	if (jQuery(e.target).is("input") === false) {
			jQuery(".class_of_form :input").each(function(i,Element)  {
				if(jQuery.trim(jQuery(Element).val()).length < 1){
					if(jQuery(this).parent().siblings().hasClass("shift")){
						jQuery(this).parent().siblings().removeClass("shift");
					}
				}
			});
		}
});
